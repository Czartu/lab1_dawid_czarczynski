﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lab1_czarczynski.Rest.Context;
using Lab1_czarczynski.Rest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Lab1_czarczynski.Rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {

        private PeopleDbContext _dbContext;

        public PeopleController(PeopleDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public IEnumerable<Person> GetAll()
        {
            return _dbContext.People;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetOne(int id)
        {
            var person = await _dbContext.People.FindAsync(id);

            if (person == null)
            {
                return NotFound();
            }

            return person;
        }

        [HttpPost]
        public async Task<ActionResult<Person>> Post(Person person) 
        {
            _dbContext.People.Add(person);
            await _dbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(GetOne), new { id = person.PersonId }, person);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Person person)
        {
            if (id != person.PersonId)
            {
                return BadRequest();
            }

            _dbContext.Entry(person).State = EntityState.Modified;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> Delete(int id)
        {
            var person = await _dbContext.People.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }

            _dbContext.People.Remove(person);
            await _dbContext.SaveChangesAsync();

            return person;
        }

        private bool PersonItemExists(int id)
        {
            return _dbContext.People.Find(id) != null;
        }
    }
}
