using Lab1_czarczynski.Rest.Models;
using Microsoft.EntityFrameworkCore;

namespace Lab1_czarczynski.Rest.Context
{
    public class PeopleDbContext : DbContext
    {

        public PeopleDbContext(DbContextOptions<PeopleDbContext> options): base(options)
        {
        }

        protected PeopleDbContext() 
        {
        }

        public DbSet<Person> People { get; set; }
        
    }
}